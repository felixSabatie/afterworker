# Afterworker
## Simple installation
Requirements :

- Docker
- Docker-compose

To run the project, go to the `docker` folder and run the following command :
- `docker-compose build && docker-compose up`

Additionally, if you don't have a `master.key` file in the config folder, or if 
the server has a 500 error when trying to create an account or log in, do the following steps :
- delete the `config/credentials.yml.enc` file
- run `docker exec -e EDITOR="nano" docker_rails rails credentials:edit` while the docker containers are still running
- stop and rerun the docker containers by running `docker-compose up` in the docker folder

It should create a `master.key` file and update the `credentials.yml.enc` file in the config folder. 
Please don't push this new credentials file, or it will break the gitlab and heroku servers.

If no errors are shown, you shoud be able to access the website at `localhost:3000`. The realtime aspects of the application won't work, as the frontend will try to connect to the production's reatime service. To make it work, you will have to do the development installation.

## Development installation
Follow the simple installation guide then do the following :

- Make sure you have nodejs and npm installed on your computer
- Go to the `frontend` folder
- run `npm install`
- run `ng serve` to launch the app. It should now be accessible at `localhost:4200`, and it will refresh in real time if you make any modification

## App architecture
- The unit tests for the ruby on rails API are located in the `spec` folder
- The angular client is located in the `frontend folder`
- The realtime part of the application is handled by an express server under the `rt-service` folder
